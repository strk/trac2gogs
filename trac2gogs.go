// Copyright 2016 by Sandro Santilli <strk@kbt.io>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
// 02110-1301  USA

package main

import (
	"crypto/sha1"
	"database/sql"
	"encoding/hex"
	"fmt"
	_ "github.com/lib/pq"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"strings"

	"github.com/go-ini/ini"
	"github.com/spf13/pflag"
)

var defaultAuthorID int64
var defaultAssigneeID int64

var tracdb, gogsdb *sql.DB
var gogsRepoID int64
var tracFilesPath string
var gogsRootPath string
var gogsCfg *ini.File

func main() {

	labelColorComponent := "#fbca04"
	labelColorPriority := "#207de5"
	labelColorSeverity := "#eb6420"
	labelColorVersion := "#009800"
	labelColorType := "#e11d21"
	labelColorResolution := "#9e9e9e"

	defaultAssignee := pflag.String("default-assignee", "",
		"`username` to assign tickets to when trac assignee is not found in Gogs")
	defaultAuthor := pflag.String("default-author", "",
		"`username` to attribute content to when trac author is not found in Gogs")

	tracFilesPathParam := pflag.String("trac-files", "",
		"`path` to trac's files directory")
	gogsRootPathParam := pflag.String("gogs-root", "",
		"`path` to gogs's root directory")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr,
			"Usage: %s [options] <trac_db> <gogs_db> <gogs_repo>\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "Options:\n")
		pflag.PrintDefaults()
	}

	pflag.Parse()

	if pflag.NArg() < 3 {
		pflag.Usage()
		os.Exit(1)
	}

	var err error

	tracDbName := pflag.Arg(0)
	gogsDbName := pflag.Arg(1)
	gogsRepoName := pflag.Arg(2)

	// Check that paths are given both or none,
	// and that they are valid if given
	if *tracFilesPathParam != "" {
		if *gogsRootPathParam == "" {
			log.Fatal("Trac files and gogs data paths must be given both or none")
		}
		stat, err := os.Stat(*tracFilesPathParam)
		if err != nil {
			log.Fatal(err)
		}
		if stat.IsDir() != true {
			log.Fatal("Trac files path is not a directory: ", *tracFilesPathParam)
		}
		stat, err = os.Stat(*gogsRootPathParam)
		if err != nil {
			log.Fatal(err)
		}
		if stat.IsDir() != true {
			log.Fatal("Gogs root path is not a directory: ", *gogsRootPathParam)
		}
		tracFilesPath = *tracFilesPathParam
		gogsRootPath = *gogsRootPathParam
	} else if *gogsRootPathParam != "" {
		log.Fatal("Trac files and gogs data paths must be given both or none")
	} else {
		log.Println("WARNING: attachments will not be migrated, add --gogs-data and --trac-files for that")
	}

	tracdb, err = sql.Open("postgres", "dbname="+tracDbName)
	if err != nil {
		log.Fatal(err)
	}

	gogsdb, err = sql.Open("postgres", "dbname="+gogsDbName)
	if err != nil {
		log.Fatal(err)
	}

	gogsRepoID = findGogsRepoID(gogsdb, gogsRepoName)

	fmt.Printf("Importing trac db '%s' to repo '%s' (%d) in gogs db '%s'\n",
		tracDbName, gogsRepoName, gogsRepoID, gogsDbName)

	adminUser := findGogsAdminUser(gogsdb)
	adminUserID := findGogsUser(gogsdb, adminUser)

	defaultAssigneeID = adminUserID
	if *defaultAssignee == "" {
		*defaultAssignee = adminUser
	} else {
		defaultAssigneeID = findGogsUser(gogsdb, *defaultAssignee)
		if defaultAssigneeID == -1 {
			log.Fatal("Cannot find gogs user ", *defaultAssignee)
		}
	}

	defaultAuthorID = adminUserID
	if *defaultAuthor == "" {
		*defaultAuthor = adminUser
	} else {
		defaultAuthorID = findGogsUser(gogsdb, *defaultAuthor)
		if defaultAssigneeID == -1 {
			log.Fatal("Cannot find gogs user ", *defaultAuthor)
		}
	}

	fmt.Println("Default Gogs assignee is", *defaultAssignee, "(", defaultAssigneeID, ")")
	fmt.Println("Default Gogs author is", *defaultAuthor, "(", defaultAuthorID, ")")

	// Import components as label "Component / <name>"

	rows, err := tracdb.Query("SELECT name FROM component")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		lbl := "Component / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorComponent)
	}

	// Import priorities as label "Priority / <name>"

	rows, err = tracdb.Query("SELECT distinct priority FROM ticket")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		lbl := "Priority / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorPriority)
	}

	// Import severities as label "Severity / <name>"

	rows, err = tracdb.Query("SELECT distinct severity FROM ticket")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		lbl := "Severity / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorSeverity)
	}

	// Import versions as label "Version / <name>"

	rows, err = tracdb.Query(`SELECT distinct version FROM ticket UNION
SELECT name from version`)
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		if val == "" {
			continue
		}
		lbl := "Version / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorVersion)
	}

	// Import types as label "Type / <name>"

	rows, err = tracdb.Query(`SELECT distinct type FROM ticket`)
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		if val == "" {
			continue
		}
		lbl := "Type / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorType)
	}

	// Import resolutions as label "Resolution / <name>"

	rows, err = tracdb.Query(`
SELECT DISTINCT resolution FROM ticket WHERE trim(resolution) != '';
`)
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var val string
		if err := rows.Scan(&val); err != nil {
			log.Fatal(err)
		}
		if val == "" {
			continue
		}
		lbl := "Resolution / " + val
		fmt.Println("Adding label", lbl)
		addLabel(gogsdb, gogsRepoID, lbl, labelColorResolution)
	}

	// Import milestones

	// NOTE: trac timestamps are to the microseconds, we just need seconds
	rows, err = tracdb.Query(`SELECT name,
int8(due*1e-6), int8(completed*1e-6), description FROM
milestone UNION select distinct(milestone),0,0,'' FROM ticket
WHERE milestone not in ( select name from milestone ) and milestone != ''`)
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var completed, due int64
		var nam, desc string
		if err := rows.Scan(&nam, &due, &completed, &desc); err != nil {
			log.Fatal(err)
		}
		fmt.Println("Adding milestone", nam)
		addMilestone(gogsdb, gogsRepoID, nam, desc, completed != 0, due, completed)
	}

	// Import tickets

	// NOTE: trac timestamps are to the microseconds, we just need seconds
	rows, err = tracdb.Query(`SELECT
t.id,t.type,
int8(t.time*1e-6),
t.component, t.severity, t.priority,
COALESCE(t.owner,''), t.reporter,
COALESCE(t.version,''),
COALESCE(t.milestone,''),
lower(COALESCE(t.status, '')),
COALESCE(t.resolution,''),
COALESCE(t.summary, ''),
COALESCE(t.description, '')
FROM ticket t order by id`)
	if err != nil {
		log.Fatal(err)
	}
	count := 0
	closedCount := 0
	for rows.Next() {
		var id, cre int64
		var comp, typ, sev, pri, own, rep, ver, mlst, stat, res, sum, desc string
		if err := rows.Scan(&id, &typ, &cre, &comp, &sev, &pri, &own, &rep,
			&ver, &mlst, &stat, &res, &sum, &desc); err != nil {
			log.Fatal(err)
		}
		fmt.Println("Adding ticket", id, " - ", sum)
		count++
		closed := stat == "closed"
		if closed {
			closedCount++
		}
		addTicket(gogsdb, gogsRepoID, id, typ, cre, comp, sev, pri, own,
			rep, ver, mlst, closed, res, sum, desc)
	}

	// Update issue count for repo
	if count > 0 {
		_, err = gogsdb.Exec(`UPDATE repository SET num_issues = num_issues+$1
	WHERE id = $2`, count, gogsRepoID)
		if err != nil {
			log.Fatal(err)
		}
	}
	if closedCount > 0 {
		_, err = gogsdb.Exec(`UPDATE repository
	SET num_closed_issues = num_closed_issues+$1
	WHERE id = $2`, closedCount, gogsRepoID)
		if err != nil {
			log.Fatal(err)
		}
	}

	// TODO: Update issue count for new labels

}

// @param prefix is the prefix to a link, like "ticket:5" for
//        a link like "ticket:5:inside_the_nest.png"
func resolveTracLink(link, prefix string) string {
	db := gogsdb
	if strings.HasPrefix(link, "http") {
		return link
	}
	if strings.HasPrefix(prefix, "ticket:") {
		tid := strings.Replace(prefix, "ticket:", "", -1)
		var gid int64
		// Find issue id
		err := db.QueryRow(`
select id from issue where repo_id = $1 and index = $2
			`, gogsRepoID, tid).Scan(&gid)
		if err != nil {
			log.Fatal(err)
		}
		// Find attachment
		var uuid string
		err = db.QueryRow(`
select uuid from attachment where issue_id = $1 and name = $2
			`, gid, link).Scan(&uuid)
		if err != nil {
			log.Fatal(err)
		}
		return gogsAttachmentURL(uuid)
	}
	log.Println("WARNING: cannot resolve trac link %s with prefix '%s'",
		link, prefix)
	return link
}

func tracTextToMarkdown(in, linkprefix string) string {

	out := in
	re := regexp.MustCompile("{{{([^\n]+?)}}}")
	out = re.ReplaceAllString(out, "`$1`")

	re = regexp.MustCompile("(?s){{{(.+?)}}}")
	out = re.ReplaceAllStringFunc(out, func(m string) string {
		lines := strings.Split(m, "\n")
		for i := range lines {
			l := lines[i]
			l = strings.Replace(l, "{{{", "", -1)
			l = strings.Replace(l, "}}}", "", -1)
			l = "\t" + l
			lines[i] = l
		}
		return strings.Join(lines, "\n")
	})

	re = regexp.MustCompile(`\[\[Image\(([^\)]*)\)\]\]`)
	out = re.ReplaceAllStringFunc(out, func(m string) string {
		u := re.ReplaceAllString(m, "$1")
		u = resolveTracLink(u, linkprefix)
		return fmt.Sprintf("![](%s)", u)
	})

	// TODO:
	//    body.gsub!(/\=\=\=\=\s(.+?)\s\=\=\=\=/, '### \1')
	//    body.gsub!(/\=\=\=\s(.+?)\s\=\=\=/, '## \1')
	//    body.gsub!(/\=\=\s(.+?)\s\=\=/, '# \1')
	//    body.gsub!(/\=\s(.+?)\s\=[\s\n]*/, '')
	//    body.gsub!(/\[(http[^\s\[\]]+)\s([^\[\]]+)\]/, '[\2](\1)')
	//    body.gsub!(/\!(([A-Z][a-z0-9]+){2,})/, '\1')
	//    body.gsub!(/'''(.+)'''/, '*\1*')
	//    body.gsub!(/''(.+)''/, '_\1_')
	//    body.gsub!(/^\s\*/, '*')
	//    body.gsub!(/^\s\d\./, '1.')

	return out
}

// Return -1 if user is not found
func findGogsUser(db *sql.DB, nameOrAddress string) int64 {
	if strings.Trim(nameOrAddress, " ") == "" {
		return -1
	}
	sql := `SELECT id FROM public.user WHERE lower_name = $1 or email = $1`
	var id int64
	err := db.QueryRow(sql, nameOrAddress).Scan(&id)
	if err != nil {
		return -1
	}
	return id
}

func addIssueLabel(db *sql.DB, repoID int64, issueid int64, label string) {
	_, err := db.Exec(`INSERT INTO issue_label(issue_id, label_id)
  SELECT $1, (SELECT id FROM label where repo_id = $2 and name = $3)`,
		issueid, repoID, label)
	if err != nil {
		log.Fatal(err)
	}
}

func addTicketAttachment(gid, tid, tim, sz int64, author, fname, desc string) string {

	db := gogsdb

	comment := fmt.Sprintf("**Attachment** %s (%d bytes) added\n\n%s",
		fname, sz, desc)
	cid := addTicketComment(gid, tid, tim, author, comment)

	tracpath := tracAttachmentPath(tid, fname)
	_, err := os.Stat(tracpath)
	if err != nil {
		log.Fatal(err)
	}

	elems := strings.Split(tracpath, "/")
	tracdir := elems[len(elems)-2]
	tracfil := elems[len(elems)-1]
	// 78ac is l33t for trac (horrible, I know)
	uuid := fmt.Sprintf("000078ac-%s-%s-%s-%s",
		tracdir[0:4], tracdir[4:8], tracdir[8:12],
		tracfil[0:12])
	gogspath := gogsAttachmentPath(uuid)
	// TODO: use a different uuid if file exists ?

	// TODO: avoid inserting record if uuid exist !
	_, err = db.Exec(`INSERT INTO attachment(
uuid, issue_id, comment_id, name, created_unix
) VALUES ($1, $2, $3, $4, $5)`, uuid, gid, cid, fname, tim)
	if err != nil {
		log.Fatal(err)
	}

	err = os.MkdirAll(path.Dir(gogspath), 0775)
	if err != nil {
		log.Fatal(err)
	}

	err = copyFile(tracpath, gogspath)
	if err != nil {
		log.Fatal(err)
	}

	return uuid
}

// comment is given in trac format
// returns gogs comment identifier
func addTicketComment(gid, tid, time int64, author, comment string) int64 {

	var header []string
	db := gogsdb

	prefix := fmt.Sprintf("ticket:%d", tid)
	comment = tracTextToMarkdown(comment, prefix)

	// find users first, and tweak description to add missing users
	authorID := findGogsUser(db, author)
	if authorID == -1 {
		header = append(header, fmt.Sprintf("    Original comment by %s", author))
		authorID = defaultAuthorID
	}
	if len(header) > 0 {
		comment = fmt.Sprintf("%s\n\n%s", strings.Join(header, "\n"), comment)
	}

	row := db.QueryRow(`INSERT INTO comment(
type, issue_id, poster_id, content, created_unix, updated_unix)
  VALUES ( 0, $1, $2, $3, $4, $4 ) RETURNING id`,
		gid, authorID, comment, time)

	var cid int64
	err := row.Scan(&cid)
	if err != nil {
		log.Fatal(err)
	}

	return cid
}

// description is given in trac format
func addTicket(db *sql.DB, repoID int64,
	id int64, typ string, created int64,
	component string, severity string,
	priority string, owner string,
	reporter string, version string,
	milestone string, closed bool,
	resolution string, summary string,
	description string) {

	description = tracTextToMarkdown(description, "")

	var header []string

	// find users first, and tweak description to add missing users
	reporterID := findGogsUser(db, reporter)
	if reporterID == -1 {
		header = append(header, fmt.Sprintf("    Originally reported by %s", reporter))
		reporterID = defaultAuthorID
	}
	var ownerID sql.NullString
	if owner != "" {
		tmp := findGogsUser(db, owner)
		if tmp == -1 {
			header = append(header, fmt.Sprintf("    Originally assigned to %s", owner))
			ownerID.String = fmt.Sprintf("%d", defaultAssigneeID)
			ownerID.Valid = true
		} else {
			ownerID.String = fmt.Sprintf("%d", tmp)
			ownerID.Valid = true
		}
	} else {
		ownerID.Valid = false
	}
	if len(header) > 0 {
		description = fmt.Sprintf("%s\n\n%s", strings.Join(header, "\n"), description)
	}

	row := db.QueryRow(`INSERT INTO issue(index, repo_id, name, poster_id,
milestone_id, assignee_id, is_pull, is_closed, content, created_unix)
SELECT $1, $2, $3, $4,
(SELECT id FROM public.milestone WHERE repo_id = $2 AND name = $5),
$6,
false, $7, $8, $9 RETURNING id`,
		id, repoID, summary, reporterID, milestone, ownerID,
		closed, description, created)

	var gid int64
	err := row.Scan(&gid)
	if err != nil {
		log.Fatal(err)
	}

	// Add labels

	var lbl string

	if component != "" {
		lbl = "Component / " + component
		addIssueLabel(db, repoID, gid, lbl)
	}

	if severity != "" {
		lbl = "Severity / " + severity
		addIssueLabel(db, repoID, gid, lbl)
	}

	if priority != "" {
		lbl = "Priority / " + priority
		addIssueLabel(db, repoID, gid, lbl)
	}

	if version != "" {
		lbl = "Version / " + version
		addIssueLabel(db, repoID, gid, lbl)
	}

	if resolution != "" {
		lbl = "Resolution / " + resolution
		addIssueLabel(db, repoID, gid, lbl)
	}

	if typ != "" {
		lbl = "Type / " + typ
		addIssueLabel(db, repoID, gid, lbl)
	}

	lastupdate := created

	// Add ticket attachments if requested

	if tracFilesPath != "" {

		rows, err := tracdb.Query(`
  SELECT
   int8(time*1e-6) tim,
   COALESCE(author, '') author,
   filename,
   description,
   size
  FROM attachment
  WHERE type = 'ticket'
  AND id = $1
  ORDER BY time asc
    `, id)
		if err != nil {
			log.Fatal(err)
		}

		for rows.Next() {
			var time, size int64
			var author, fname, desc string
			if err := rows.Scan(&time, &author, &fname, &desc, &size); err != nil {
				log.Fatal(err)
			}
			fmt.Println(" adding attachment by", author)
			if lastupdate > time {
				lastupdate = time
			}
			addTicketAttachment(gid, id, time, size, author, fname, desc)
		}

	}

	// Add ticket comments

	rows, err := tracdb.Query(`
SELECT
 int8(time*1e-6) tim,
 COALESCE(author, '') author,
 COALESCE(newvalue, '') newval
FROM ticket_change where ticket = $1
AND field = 'comment'
AND trim(COALESCE(newvalue, ''), ' ') != ''
ORDER BY time asc
  `, id)
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var time int64
		var author, comment string
		if err := rows.Scan(&time, &author, &comment); err != nil {
			log.Fatal(err)
		}
		fmt.Println(" adding comment by", author)
		if lastupdate > time {
			lastupdate = time
		}
		addTicketComment(gid, id, time, author, comment)
	}

	// Update issue modification time
	_, err = db.Exec(`UPDATE issue SET updated_unix = greatest(updated_unix,$1)
	WHERE id = $2`, lastupdate, gid)
	if err != nil {
		log.Fatal(err)
	}

}

func addMilestone(db *sql.DB, repoID int64, name string, content string, closed bool, deadlineTS int64, closedTS int64) {
	_, err := db.Exec(`INSERT INTO
milestone(repo_id,name,content,is_closed,deadline_unix,closed_date_unix)
SELECT $1,$2::varchar,$3,$4,$5,$6 WHERE
NOT EXISTS ( SELECT * FROM milestone WHERE repo_id = $1 AND name = $2 )`,
		repoID, name, content, closed, deadlineTS, closedTS)
	if err != nil {
		log.Fatal(err)
	}
}

func addLabel(db *sql.DB, repoID int64, label string, color string) {
	_, err := db.Exec(`INSERT INTO label(repo_id,name,color)
SELECT $1,$2::varchar, $3 WHERE
NOT EXISTS ( SELECT * FROM label WHERE repo_id = $1 AND name = $2 )`,
		repoID, label, color)
	if err != nil {
		log.Fatal(err)
	}
}

func findGogsAdminUser(db *sql.DB) string {
	sql := `
select lower_name from public.user where is_admin order by id limit 1;
`
	row := db.QueryRow(sql)

	var name string
	err := row.Scan(&name)
	if err != nil {
		fmt.Fprintf(os.Stderr, "No adming user found in Gogs\n")
		os.Exit(1)
	}

	return name
}

func findGogsRepoID(db *sql.DB, fullname string) int64 {

	comps := strings.Split(fullname, "/")
	if len(comps) < 2 {
		log.Fatal("Invalid gogs repo name (missing '/'): " + fullname)
	}
	owner := comps[0]
	repo := comps[1]

	sql := `
SELECT r.id FROM public.repository r, public.user u WHERE r.owner_id =
u.id AND u.name = $1 AND r.name = $2
`
	row := db.QueryRow(sql, owner, repo)

	var id int64
	err := row.Scan(&id)
	if err != nil {
		fmt.Fprintf(os.Stderr, "No "+fullname+" repository found in Gogs\n")
		os.Exit(1)
	}

	return id
}

func gogsGetConfig() *ini.File {
	if gogsCfg == nil {
		p := fmt.Sprintf("%s/custom/conf/app.ini", gogsRootPath)
		var err error
		gogsCfg, err = ini.Load(p)
		if err != nil {
			log.Fatal(err)
		}
	}
	return gogsCfg
}

func gogsAttachmentURL(uuid string) string {
	baseURL, err := gogsGetConfig().Section("server").GetKey("ROOT_URL")
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%s/attachments/%s", baseURL, uuid)
}

func gogsAttachmentPath(uuid string) string {
	d1 := uuid[0:1]
	d2 := uuid[1:2]
	// TODO: seek for PATH under [attachment]
	//       in gogsRootPath/custom/conf/app.ini
	subpath := "data/attachments"
	return fmt.Sprintf("%s/%s/%s/%s/%s", gogsRootPath, subpath, d1, d2, uuid)
}

func tracAttachmentPath(tid int64, fname string) string {
	ticketDir := encodeSha1(fmt.Sprintf("%d", tid))
	ticketSub := ticketDir[0:3]

	pathFile := encodeSha1(fname)
	pathExt := path.Ext(fname)

	return fmt.Sprintf("%s/attachments/ticket/%s/%s/%s%s", tracFilesPath, ticketSub, ticketDir, pathFile, pathExt)
}

// Encode string to sha1 hex value.
func encodeSha1(str string) string {
	h := sha1.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

func copyFile(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, in)
	cerr := out.Close()
	if err != nil {
		return err
	}
	return cerr
}
