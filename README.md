# Migrate trac tickets to gogs

Homepage: http://strk.kbt.io/projects/go/trac2gogs/
Contact: strk@kbt.io

NOTE: only supported when both Gogs and Trac use an PostgreSQL database

WARNING: this is a work in progress, not ready for production !

Done:

 - Imports Components, Priorities, Severities, Versions, Types
   and Resolutions to labels
 - Imports Milestones
 - Import of tickets
 - Import ticket comments
 - Convert {{{ }}} blocks to ``` ```
 - Import attachments
 - Convert traclinks

Todo:

 - Import wiki pages ?
