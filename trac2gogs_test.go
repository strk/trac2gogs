package main

import "testing"

func TestTracToMarkdown(t *testing.T) {

	cases := []struct{ in, want string }{
		{"say {{{hello}}} {{{world}}}", "say `hello` `world`"},
		{
			`say {{{
hello
}}}
{{{
world
}}}`,
			`say 	
	hello
	
	
	world
	`},
	}

	for _, c := range cases {
		got := tracTextToMarkdown(c.in, "")
		if got != c.want {
			t.Errorf("tracTextToMarkdown(%q) == %q, expected %q", c.in, got, c.want)
		}
	}

}
